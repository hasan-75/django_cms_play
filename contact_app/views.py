from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.urls import reverse_lazy
from django.views.generic import CreateView
from contact_app.models import ContactMessage
from djangocms_site.settings import ADMIN_EMAIL_LIST
import _thread as thread


def send_email_faster(message):
    message.send(fail_silently=False)


class ShowContactForm(SuccessMessageMixin, CreateView):
    model = ContactMessage
    fields = '__all__'
    template_name = 'contact_app/contact_form.html'
    success_url = reverse_lazy('contact_app:contact_form')
    success_message = 'Message sent!'

    def post(self, request, *args, **kwargs):
        try:
            message = EmailMessage(
                subject='Message from ' + request.POST['name'],
                body=request.POST['message'],
                from_email=request.POST['sender_email'],
                to=ADMIN_EMAIL_LIST
            )
            try:
                file = request.FILES['file']
                message.attach(file.name, file.read(), file.content_type)
            except:
                pass

            try:
                thread.start_new_thread(send_email_faster, (message,))
            except:
                raise Exception("Problem in thread")

            message = EmailMessage(
                subject='Confirmation from ' + get_current_site(request).name,
                body='Dear {}, \nThanks for your message. Stay tuned'.format(request.POST['name']),
                from_email=get_current_site(request).name,
                to=[request.POST['sender_email'], ],
            )

            try:
                thread.start_new_thread(send_email_faster, (message,))
            except:
                raise Exception("Problem in thread")
            return super().post(request, args, kwargs)
        except:
            raise Exception('error sending email')
