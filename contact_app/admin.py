from django.contrib import admin

# Register your models here.
from contact_app.models import ContactMessage

admin.site.register(ContactMessage)