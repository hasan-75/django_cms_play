from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

@apphook_pool.register
class Contact_App_Apphook(CMSApp):
    app_name = 'contact_app'
    name = 'Contact with admin'

    def get_urls(self, page=None, language=None, **kwargs):
        return ['contact_app.urls']