from django.conf.urls import url
from django.conf.urls.static import static

from contact_app.views import ShowContactForm
from djangocms_site import settings

app_name = 'contact_app'
urlpatterns = [
    url(r'^$', ShowContactForm.as_view(), name='contact_form'),
    ]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
