# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-04-08 09:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contact_app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='contactmessage',
            name='file',
            field=models.FileField(blank=True, null=True, upload_to=''),
        ),
    ]
