# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-04-09 12:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contact_app', '0002_contactmessage_file'),
    ]

    operations = [
        migrations.AddField(
            model_name='contactmessage',
            name='name',
            field=models.CharField(default=None, max_length=200),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='contactmessage',
            name='file',
            field=models.FileField(blank=True, null=True, upload_to='uploads/'),
        ),
    ]
