from audioop import reverse

from django.db import models


# Create your models here.
from django.utils.safestring import mark_safe


class ContactMessage(models.Model):
    name = models.CharField(max_length=200)
    sender_email = models.EmailField(max_length=200, verbose_name='Sender\'s email')
    message = models.TextField()
    file = models.FileField(blank=True, null=True, upload_to='uploads/')
    def __str__(self):
        return mark_safe("<b>From:</b> "+self.sender_email+"<br>"+self.message[:20]+"...")

