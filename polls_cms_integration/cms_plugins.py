from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from polls.models import Poll

from polls_cms_integration.models import PollListPluginModel, MostPopularPollPluginModel
from django.utils.translation import ugettext as _


@plugin_pool.register_plugin  # register the plugin
class PollPopularPluginPublisher(CMSPluginBase):
    model = MostPopularPollPluginModel  # model where plugin data are saved
    module = _("Polls")
    name = _("Popular Poll")  # name of the plugin in the interface
    render_template = "polls_cms_integration/popular_poll_plugin.html"

    def render(self, context, instance, placeholder):
        popular_pole = instance.getPop()[0]
        vote_count = instance.getPop()[1]
        context.update({'instance': instance, 'poll': popular_pole, 'vote_count': vote_count,})
        return context

@plugin_pool.register_plugin
class PollListPluginPublisher(CMSPluginBase):
    model = PollListPluginModel
    module = _("Polls")
    name = _("Poll Plugin List")  # name of the plugin in the interface
    render_template = "polls_cms_integration/poll_plugin_list.html"

    def render(self, context, instance, placeholder):
        context.update({'instance': instance, 'polls':instance.get_all()})
        #print(Poll.objects.all())
        return context
