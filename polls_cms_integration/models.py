from django.db import models
from cms.models import CMSPlugin
from polls.models import Poll


class PollListPluginModel(CMSPlugin):
    label = models.CharField(max_length=200)

    def get_all(self):
        return Poll.objects.all
    def __str__(self):
        return self.label


class MostPopularPollPluginModel(CMSPlugin):

    label = models.CharField(max_length=200)

    def getPop(self):
        polls = Poll.objects.all()
        popular_pole = polls[0]
        prev_vote_count = 0
        for p in polls:
            vote_count = 0
            for choice in p.choice_set.all():
                vote_count += int(choice.votes)
            if vote_count >= prev_vote_count:
                popular_pole = p
                prev_vote_count = vote_count

        return popular_pole, prev_vote_count


    def __str__(self):
        return self.label